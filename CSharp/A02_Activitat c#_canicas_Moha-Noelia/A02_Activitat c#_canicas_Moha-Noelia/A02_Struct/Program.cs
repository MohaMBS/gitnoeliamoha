﻿using System;

namespace A02
{
    class Program
    {
      struct neneCanicas {
          //Aquestes són les diferents estructures que es compon el programa.
        public string nombre;
        public int    canicas;
        public string ciudad;
    } 
    static void menu(){
        //Menu principal per fer les operacions.
            Console.Write(@"Aplicació de caniques::
            1. Consulta de caniques.
            2. Canvi de nom.
            3. Lugar d'on vens.
            4. Canviar d'usuari:
            0. Salir.
            Què vols fer: ");
        }
    
    //Funció per consultar quantes caniques te el nen identificat.
    static void consultacanicas(neneCanicas[] partidas, int posicionPersona){
            Console.WriteLine("El teu número de caniques és: " +partidas[posicionPersona].canicas);
            canviarCanicas(partidas, posicionPersona);
        }
    
    //Funció per canviar el nom dels nens
    static void canviarNom(neneCanicas[] partidas, int posicionPersona){
            string nombreNuevo;//En aques el que fem es declarar una nova variable string per poder guradar el nou nom
            Console.WriteLine("Escriu el nou nom per fer el canvi: ");
            nombreNuevo= Console.ReadLine();
            partidas[posicionPersona].nombre=nombreNuevo.ToUpper();// Amb la posicio pasada per cridar a la funcio del nom l'assignem al nou nom del nen/a i el modifiquem.
            Console.WriteLine("El teu nom s'ha canviat a ");
            Console.WriteLine(partidas[posicionPersona].nombre);// Printa el nom que té ara ek nen/a.
        }
        
        //Funcio per canviar les caniques de cada nen
        static void canviarCanicas(neneCanicas[] partidas, int posicionPersona){
            int opcion;
            int cantidad;
            opcion=0;
            cantidad=0;
            Console.WriteLine(@"Menu canicas:
            1.Afegir cacnicas.
            2.Treure canicas.
            3.Sortir.
            Escull el que vos fer:");
            opcion=Int32.Parse(Console.ReadLine());
            switch (opcion){
                case 1: //En aquest cas s'afegeixen caniques
                    Console.WriteLine("Posa la quantitat de caniques que vols introduir:");
                    cantidad=Int32.Parse(Console.ReadLine());
                    if (cantidad > 0){ //Hacemos un control para que solo pueda ingresar mayor a 0
                        partidas[posicionPersona].canicas=partidas[posicionPersona].canicas+cantidad;
                        Console.WriteLine("Ara tens un total de "+partidas[posicionPersona].canicas);
                    }else{
                        Console.WriteLine("No es pot afegir 0");
                    }
                    break;
                case 2://En aquest cas es treuen caniques
                    Console.WriteLine("Posa la quantitat de caniques que vols introduir: ");
                    cantidad=Int32.Parse(Console.ReadLine());
                    if (cantidad > 0){ //Hacemos un control para que solo pueda ingresar mayo 0
                        if(partidas[posicionPersona].canicas< cantidad){
                            Console.WriteLine("No pots truere més caniques de les que tens.\n");
                        }
                        else{
                            partidas[posicionPersona].canicas=partidas[posicionPersona].canicas-cantidad;
                            Console.WriteLine("Ara tens un total de "+partidas[posicionPersona].canicas);}
                    }else{
                        Console.WriteLine("No es pot treure 0 caniques.");
                    }
                    break;
                case 3: //sortir del bucle
                    break;
                default:
                        Console.WriteLine("Aquesta opcio no existeix!\nSortint del menu de canicas...");
                    break;
            }
        }

        //Funció per dir d'on ve cada nen/a
        static void donVens(neneCanicas[] partidas, int posicionPersona){
            Console.WriteLine("Vens de: " +partidas[posicionPersona].ciudad);
        }

    //Funció per averigurar de quien nen/a es tracta.       
    static int quineEres(neneCanicas[] partidas){
            string nom;
            nom="";
            Boolean autenOk;//Per saber si se ha autenticat de forma correcta
            int indentificador;
            indentificador=-1;//Aquesta variable s'utilitza para emmagatzemar la posició del nen quan es troba.
            autenOk=false;
            Console.WriteLine("Posa el teu nom per identificar qui ets: ");
            nom=Console.ReadLine();
            for (int x =0; x < partidas.Length ; x++ ){
                if( nom.ToUpper() == partidas[x].nombre){//Aqui recorrem totes les posicions del nens fins que donemamb  nom i quan coincideixi l'assignem la (x) a l'indentificador
                    autenOk=true;
                    indentificador=x;
                }
            }
            if (autenOk==true && indentificador>=0){
                Console.WriteLine("Hola! "+partidas[indentificador].nombre+" Benvingut...");
                return (indentificador);//Si tot és correcte es torna la posició del nen/a
            }else{

                return -1;//Si no, un menos -1 el cual provacara que le pregunte otra vez que quien es
            }
        }
        static void Main(string[] args)
        {
            //Identificació de tots els nen/a , per saber com es diuen, les caniques què tenen i d'on venen.
            
            neneCanicas[] partida = new neneCanicas[8];
            partida[0].nombre="JOAN";
            partida[0].canicas=12;
            partida[0].ciudad="VILANOVA";

            partida[1].nombre="NIL";
            partida[1].canicas=3;
            partida[1].ciudad="CANYELLES";

            partida[2].nombre="ROC";
            partida[2].canicas=25;
            partida[2].ciudad="VILANOVA";

            partida[3].nombre="THIAGO";
            partida[3].canicas=31;
            partida[3].ciudad="SITGES";

            partida[4].nombre="NICO";
            partida[4].canicas=6;
            partida[4].ciudad="CUNIT";

            partida[5].nombre="NOA";
            partida[5].canicas=11;
            partida[5].ciudad="VILANOVA";

            partida[6].nombre="GERARD";
            partida[6].canicas=21;
            partida[6].ciudad="KAZAHKSTAN";

            partida[7].nombre="NOR";
            partida[7].canicas=9;
            partida[7].ciudad="CUBELLAS";

            int posicionPersona;
            posicionPersona=-1; //cambiar a -1
            while (posicionPersona == -1)
            {
                posicionPersona=quineEres(partida);
                /* posicionPersona=quineEres(ref nomUsuari, ref posicionPersona); */
                Boolean salir;
                salir=false;
                while(posicionPersona >= 0 && salir ==false){
                    menu();
                    string numero= Console.ReadLine();
                    int opcio = Int32.Parse(numero);
                    switch(opcio){
                        case 0:
                            Console.WriteLine("ADEU!!!");
                            salir=true;
                            break;
                        case 1:
                             consultacanicas(partida, posicionPersona);
                            break;
                        case 2:
                            canviarNom(partida, posicionPersona);
                            break;
                        case 3:
                            donVens(partida, posicionPersona);
                            break;
                        case 4:
                            posicionPersona=quineEres(partida);
                            break;
                        default:
                            Console.WriteLine("Aquesta opció no existeix!\nIntrodueix un altre.");
				        break; }
                }
            }
        }
    }
}
