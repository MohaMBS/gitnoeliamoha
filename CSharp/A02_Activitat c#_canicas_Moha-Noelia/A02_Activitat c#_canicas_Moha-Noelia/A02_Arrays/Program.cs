﻿using System;

namespace A02
{
    class Program
    {
        //Menu principal per fer les operacions.
        static void menu(){
            Console.Write(@"Aplicació de caniques:
            1. Consulta de caniques
            2. Canvi de nom
            3. Lugar d'on vens.
            4. Canviar d'usuari.
            0. Sortir.
            Què vols fer: ");
        }
        //Funció per canviar el nom dels nens
        public static void canviarNom(ref string[] nomUsuari, int posicionPersona){
            string nombreNuevo;//En aques el que fem es declarar una nova variable string per poder guradar el nou nom
            Console.WriteLine("Escriu el nou nom per fer el canvi: ");
            nombreNuevo= Console.ReadLine();
            nomUsuari[posicionPersona]=nombreNuevo.ToUpper();// AMb la posicio pasada per cridar a la funcio del nom l'assignem al nou nom del nen/a i el modifiquem.
            Console.WriteLine("El teu nom s'ha canviat a " + nomUsuari[posicionPersona]);// Printa el nom que té ara ek nen/a.
        }
        //Funcio per canviar les caniques de cada nen
        public static void canviarCanicas(ref int[] canicasUsuari, int posicionPersona){
            int opcion;
            int cantidad;
            opcion=0;
            cantidad=0;
            //Submenu de les caniques
            Console.WriteLine(@"Menu canicas:
            1.Afegir caniques.
            2.Treure caniques.
            3.Sortir.
            Escull el que vos fer: ");
            opcion=Int32.Parse(Console.ReadLine());
            switch (opcion){
                case 1: //En aquest cas s'afegeixen caniques
                    Console.WriteLine("Posa la quantitat de caniques que vols introduir:");
                    cantidad=Int32.Parse(Console.ReadLine());
                    if (cantidad > 0){ //Hacemos un control para que solo pueda ingresar mayor a 0
                        canicasUsuari[posicionPersona]=canicasUsuari[posicionPersona]+cantidad;
                        Console.WriteLine("Ara tens un total de " +canicasUsuari[posicionPersona] + " caniques.");
                    }else{
                        Console.WriteLine("No es pot afegir 0 caniques");
                    }
                    break;
                case 2: //En aquest cas es treuen caniques
                    Console.WriteLine("Posa la quantitat de caniques que vols treure:");
                    cantidad=Int32.Parse(Console.ReadLine());
                    if (cantidad > canicasUsuari[posicionPersona]){ 
                        Console.Write ("No pots truere tantes caniques per treure.\n");
                    }else if (cantidad==0){
                        Console.WriteLine("No es pot treure 0 caniques.");
                    }
                    else{
                        canicasUsuari[posicionPersona]=canicasUsuari[posicionPersona]-cantidad;
                        Console.WriteLine("Ara tens un total de " +canicasUsuari[posicionPersona] + "caniques.");
                    }
                    break;
                case 3:
                    break;
                default:
                        Console.WriteLine("Aquesta opció no existeix!\nSortint del menu de caniques...");
                    break;
            }
        }
        //Funció per averigurar de quien nen/a es tracta.
            public static int quineEres(ref string[] nomUsuari, int posicionPersona){
            string nom;
            nom="";
            Boolean autenOk;//Per saber si se ha autenticat de forma correcta
            int indentificador;
            indentificador=-1;//Aquesta variable s'utilitza para emmagatzemar la posició del nen quan es troba a l'array.
            autenOk=false;
            Console.WriteLine("Posa el teu nom per identificar qui ets: ");
            nom=Console.ReadLine();
            for (int x =0; x < nomUsuari.Length ; x++ ){
                if( nom.ToUpper() == nomUsuari[x]){//Aqui recorrem l'array del nom i coincideixil'assignem la (x) a l'indentificador
                    autenOk=true;
                    indentificador=x;
                }
            }
            if (autenOk==true && indentificador>=0){
                Console.WriteLine("BIENVENIDO "+nomUsuari[indentificador]);
                return (indentificador);//Si tot és correcte es torna la posició del nen/a a l'array
            }else{

                return -1;//Si no es correcte, es retornara un -1 i preguntara un altra vegada qui és?
            }
        }
        //Funció per consultar quantes caniques te el nen registrat
        static void consultacanicas(ref int[] canicasUsuari, int posicionPersona){
            Console.WriteLine("El teu numero de caniques és de: " +canicasUsuari[posicionPersona]);
            canviarCanicas(ref canicasUsuari, posicionPersona);
        }
        //Funció per dir d'on ve cada nen/a
        static void donVens(ref string[] lugarUsuari, int posicionPersona){
            Console.WriteLine("Vens de: " +lugarUsuari[posicionPersona]);
        }
        //Funció del menú principal
        static void Main(string[] args)
        {
            string[] nomUsuari={"JOAN","NIL","ROC", "THIAGO", "NICO", "NOA", "GERARD ", "NOR "}; //correspon al nom de cada nen/a
            int[] canicasUsuari={12, 3, 25, 31, 6, 11, 21, 9 }; //correspon a les caniques de cadascú respectivament
            string[] lugarUsuari={"VILANOVA", "CANYELLES", "VILANOVA", "SITGES", "CUNIT", "VILANOVA", "KAZAHKSTAN", "CUBELLES" };//d'on venen respectivament
            int posicionPersona; // per saber en quina posicio de l'array esta cada nen, per despres poder fer les operacions.
            posicionPersona=-1;
            Boolean sortir; // variable creada per tancar el programa
            sortir= false; //Per controlar quan acaba el programa, opcio de sortir(tancar sessió).

            while (posicionPersona == -1)  { //recorro l'array de les persones per saber qui ésjoan
                posicionPersona=quineEres(ref nomUsuari, posicionPersona);
                while(sortir == false && posicionPersona >= 0){
                    menu(); //Aquest es el menu el qual executa les funcions corresponents a cada cas.
                    string numero= Console.ReadLine();
                    int opcio = Int32.Parse(numero);
                    switch(opcio){
                         // Dintre de les opcions crido a les funcions.
                        case 0:
                            sortir=true;
                            break;
                        case 1:
                            consultacanicas(ref canicasUsuari, posicionPersona);
                            break;
                        case 2:
                            canviarNom(ref nomUsuari, posicionPersona);
                            break;
                        case 3:
                            donVens(ref lugarUsuari,  posicionPersona);
                            break;
                        case 4:
                            posicionPersona=quineEres(ref nomUsuari, posicionPersona); //Per fer un canvi d'usuari per tal de poder un alytre nen fer consultes o modificar dades, sensse necessitat de finalitzar el programa per coomplert.
                            break;
                        default:
                            Console.WriteLine("Aquesta opcio no existeix!\nIntrodueix un altre.");
				        break; 
                    }
                }
            }
        }
    }
}

