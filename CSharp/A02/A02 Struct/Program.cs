﻿using System;

namespace A02
{
    class Program
    {
      struct neneCanicas
    {
        public string nombre;
        public int    canicas;
        public string ciudad;
    } 
    static void menu(){
            Console.Write(@"Aplicacio:
            1. Consulta de caniques
            2. Canvi de nom
            3. Lugar d'on vens.
            4. Canviar d'usuari:
            0. Salir.
            Què vols fer:");
        }
    static void consultacanicas(neneCanicas[] partidas, int posicionPersona){
            Console.WriteLine("El teu numero de caniques és: " +partidas[posicionPersona].canicas);
            canviarCanicas(partidas, posicionPersona);
        }
    static void canviarNom(neneCanicas[] partidas, int posicionPersona){
            string nombreNuevo;//Aqui lo que hacemos es declara una nueva variable tipo string para poder guradar el nuevo nombre
            Console.WriteLine("Escriba su nuevo nombre completo");
            nombreNuevo= Console.ReadLine();
            partidas[posicionPersona].nombre=nombreNuevo.ToUpper();//Aqui con la posicion que nos pasan al llamar la funcion del client pues asignamos la variable de nombreNuevo y asi lo modificamos
            Console.WriteLine("Su Nombre se cambio a:");
            Console.WriteLine(partidas[posicionPersona].nombre);//Un writeline para que vea como queda el nombre fina.
        }
        static void canviarCanicas(neneCanicas[] partidas, int posicionPersona){
            int opcion;
            int cantidad;
            opcion=0;
            cantidad=0;
            Console.WriteLine(@"Menu canicas:
            1.Afegir cacnicas.
            2.Treure canicas.
            3.Sortir.
            Escull el que vos fer:");
            opcion=Int32.Parse(Console.ReadLine());
            switch (opcion){
                case 1:
                    Console.WriteLine("Posa la cantidad que vols introduir:");
                    cantidad=Int32.Parse(Console.ReadLine());
                    if (cantidad > 0){ //Hacemos un control para que solo pueda ingresar mayor a 0
                        partidas[posicionPersona].canicas=partidas[posicionPersona].canicas+cantidad;
                        Console.WriteLine("Ara tens un total de "+partidas[posicionPersona].canicas);
                    }else{
                        Console.WriteLine("No es pot afegir 0");
                    }
                    break;
                case 2:
                    Console.WriteLine("Posa la cantidad que vols introduir:");
                    cantidad=Int32.Parse(Console.ReadLine());
                    if (cantidad > 0){ //Hacemos un control para que solo pueda ingresar mayo 0
                        if(partidas[posicionPersona].canicas< cantidad){
                            Console.WriteLine("No pots treure mes del que tens.");
                        }
                        else{
                            partidas[posicionPersona].canicas=partidas[posicionPersona].canicas-cantidad;
                            Console.WriteLine("Ara tens un total de "+partidas[posicionPersona].canicas);}
                    }else{
                        Console.WriteLine("No es pot treure 0");
                    }
                    break;
                case 3:
                    break;
                default:
                        Console.WriteLine("Aquesta opcio no existeix!\nSortint del menu de canicas...");
                    break;
            }
        }
        static void donVens(neneCanicas[] partidas, int posicionPersona){
            Console.WriteLine("Vens de: " +partidas[posicionPersona].ciudad);
        }    
    static int quineEres(neneCanicas[] partidas){
            string nom;
            nom="";
            Boolean autenOk;//Para saber si se ha autenticado de forma correcta
            int indentificador;
            indentificador=-1;//Esta variable se usa para almacernar la posicion del cliente cuando le encontremos
            autenOk=false;
            Console.WriteLine("Dame tu nombre para indentificar quien eres:");
            nom=Console.ReadLine();
            for (int x =0; x < partidas.Length ; x++ ){
                if( nom.ToUpper() == partidas[x].nombre){//Aqui recomremos la arry de dni y cuando coincida, asignamos la x a indentificador
                    autenOk=true;
                    indentificador=x;
                }
            }
            if (autenOk==true && indentificador>=0){
                Console.WriteLine("Hola! "+partidas[indentificador].nombre+" bienvenido...");
                return (indentificador);//Si todo fue bien devuelvo la posicion del cliente en mis arrys
            }else{

                return -1;//Si no, un menos -1 el cual provacara que le pregunte otra vez que quien es
            }
        }
        static void Main(string[] args)
        {
            
            neneCanicas[] partida = new neneCanicas[8];
            partida[0].nombre="JOAN";
            partida[0].canicas=12;
            partida[0].ciudad="VILANOVA";

            partida[1].nombre="NIL";
            partida[1].canicas=3;
            partida[1].ciudad="CANYELLES";

            partida[2].nombre="ROC";
            partida[2].canicas=25;
            partida[2].ciudad="VILANOVA";

            partida[3].nombre="THIAGO";
            partida[3].canicas=31;
            partida[3].ciudad="SITGES";

            partida[4].nombre="NICO";
            partida[4].canicas=6;
            partida[4].ciudad="CUNIT";

            partida[5].nombre="NOA";
            partida[5].canicas=11;
            partida[5].ciudad="VILANOVA";

            partida[6].nombre="GERARD";
            partida[6].canicas=21;
            partida[6].ciudad="KAZAHKSTAN";

            partida[7].nombre="NOR";
            partida[7].canicas=9;
            partida[7].ciudad="CUBELLAS";

            int posicionPersona;
            posicionPersona=-1; //cambiar a -1
            while (posicionPersona == -1)
            {
                posicionPersona=quineEres(partida);
                /* posicionPersona=quineEres(ref nomUsuari, ref posicionPersona); */
                Boolean salir;
                salir=false;
                while(posicionPersona >= 0 && salir ==false){
                    menu();
                    string numero= Console.ReadLine();
                    int opcio = Int32.Parse(numero);
                    switch(opcio){
                        case 0:
                            Console.WriteLine("ADEU!!!");
                            salir=true;
                            break;
                        case 1:
                             consultacanicas(partida, posicionPersona);
                            break;
                        case 2:
                            canviarNom(partida, posicionPersona);
                            break;
                        case 3:
                            donVens(partida, posicionPersona);
                            break;
                        case 4:
                            posicionPersona=quineEres(partida);
                            break;
                        default:
                            Console.WriteLine("Aquesta opcio no existeix!\nIntrodueix un altre.");
				        break; }
                }
            }
        }
    }
}
