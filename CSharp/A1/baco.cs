﻿using System;

namespace Banco
{
    class Program
    {
        //Creacion del menu que luego se muestra en el MAIN.
        public static void Menu(){
        Console.WriteLine(@"Bienvenido al banco de DAW.
        1.Sacar dinero.
        2.Mi cuenta.
        3.Ingresar dinero.
        4.Alta nuevo usuario.
        5.Baja de usuario.
        0.Salir.");
        }
        public static void sacarDinero(ref float[] saldoClientes, int posi){
            //Creacion de la funcion de sacar dinero
            float retira;//Declaramos la varaiable para luego sacar el dinero
            Console.WriteLine("Cuanto desea sacar?");
            retira=Convert.ToSingle(Console.ReadLine());
            if (retira <= saldoClientes[posi]){//Hacemos un control con if, para que no pueda sacar mas dinero del que tiene
                saldoClientes[posi]=saldoClientes[posi]-retira;
                Console.WriteLine(saldoClientes[posi]);
            }else{//Con el else basicamente lo que hago, es que si no dispone de la cantidad no le dejo sacar el dinero.
                Console.WriteLine("No dispone de esa cantidad para retirar...");
            }
                 
            
        }
        public static void miCuenta(ref float[] saldoClientes, ref string[] nombresClientes, ref string[] dniClientes, int posi){
            //Tambien le pasamos arrys con ref para poder modificarlas de forma "global" y el posi es la posicion del cliente en nuestras arrys para asi poder hacer las modificaciones o acciones necesarias.
            //En mi cuenta lo que hecho es que cuando llamae a la funccion des de el main pues printe un segundo menu para poder hacer otra gestiones.
            Console.WriteLine(@"Menu de tu cuenta:
            1.Consultar saldo.
            2.Consulta de informacion de cuenta.
            3.Midificacion de DNI.
            4.Modificacion de Nombre y apellido.");
            string num;
            num= Console.ReadLine();
            int opcionMenu = Int32.Parse(num);
            switch (opcionMenu){
                case 1://Como importamos las arrys con ref podremos sacar el saldo que tiene con la posicion del cliente que nos pasan en la funcion
                    Console.WriteLine("Usted dispone de un total");
                    Console.WriteLine(saldoClientes[posi]);
                    break;
                case 2://Aqui usamos la referencia de arrys de nombres y dni para poder darle la informacion solicitada
                    Console.WriteLine(@"Su informacion es.
                    Su numbre es:");
                    Console.WriteLine(nombresClientes[posi]);
                    Console.WriteLine("Su DNI es:");
                    Console.WriteLine(dniClientes[posi]);
                    break;
                case 3:
                    string dniNuevo; //Aqui lo que hacemos es declara una nueva variable tipo string para poder guradar el nuevo dni
                    Console.WriteLine("Escriba su nuevo DNI");
                    dniNuevo= Console.ReadLine();
                    dniClientes[posi]=dniNuevo; //Aqui con la posicion que nos pasan al llamar la funcion del client pues asignamos la variable de dniNuevos y asi lo modificamos
                    Console.WriteLine("Su DNI se cambio a:");
                    Console.WriteLine(dniClientes[posi]); //Un writeline para que vea como queda el dni final
                    break;
                case 4:
                    string nombreNuevo;//Aqui lo que hacemos es declara una nueva variable tipo string para poder guradar el nuevo nombre
                    Console.WriteLine("Escriba su nuevo nombre completo");
                    nombreNuevo= Console.ReadLine();
                    nombresClientes[posi]=nombreNuevo;//Aqui con la posicion que nos pasan al llamar la funcion del client pues asignamos la variable de nombreNuevo y asi lo modificamos
                    Console.WriteLine("Su Nombre se cambio a:");
                    Console.WriteLine(nombresClientes[posi]);//Un writeline para que vea como queda el nombre fina.
                    break;
            }
            
            
        }
        public static void ingresar(ref float[] saldoClientes, int posi){
            //Pasamos arrys con ref para poder midificar esas arrys de forma "global" y posi es la posicion de nuestro cliente en nuestras arrys
            float ingresar; //Creamos una variable float para poder almacenar la cantidad que quiere ingresar.
            Console.WriteLine("Cuanto desea ingresar?");
            ingresar=Convert.ToSingle(Console.ReadLine()); //Aqui se hace una conversion de string a float para poder tratar los datos de forma correcta y se la asignamos a la variable.
            if (ingresar <= 1000){ //Hacemos un control para que solo pueda ingresar 1000 o menos
                saldoClientes[posi]=saldoClientes[posi]+ingresar;//Si se complue, pues con la posicion de nuestro cliente en nuestra arry modificamos el dato que toca 
                Console.WriteLine(saldoClientes[posi]);
            }
            while (ingresar > 1000){
                // En caso de que la cantidad que quiere ingresar se meyor a 1000, no le damos y le preguntaremos tantas veces como sean necesarias hasta que ingrese una cantidad menor a 1000.
                Console.WriteLine("No se puede igresar mas de 1000€. Vuela a introducir una cantidad menor a 1000€");
                ingresar=Convert.ToSingle(Console.ReadLine());
                if (ingresar <=1000){//Aqui controlamos la cantidad que introduce y si es menor o igual a 1000 entonces le podemos asignar el dinero.
                    saldoClientes[posi]=saldoClientes[posi]+ingresar;//Si comple la concion se ara la accion del ingreso
                }
            }
            Console.WriteLine("Se ha ingresado de forma correcta.");
            
        }
        public static void altaUsusario(ref float[] saldoClientes, ref string[] nombresClientes, ref string[] dniClientes){
            //Pasamos arrys con ref para poder midificar esas arrys de forma "global" y posi es la posicion de nuestro cliente en nuestras arrys
            string nombre; //Creamos las variables necesarias para poder dar de alta
            string dni;
            Boolean registro;//Variable que usaremos para controlar el proceso de alta
            registro=false;
            for (int i =0; i < dniClientes.Length ; i++ ){//Recomremos la arry de dni para saber si hay sitio o no y si lo hay, pues añadimos los datos del usuario
                if (dniClientes[i]==""){//Si la i que es una possicion que amuenta cada vez que entra en el for, si ese campo es igual a "" pues es porque hay hueco libre para otro usuario y procederiamos con el registro 
                    Console.WriteLine("Dame tu nombre para seguir con el registro.");
                    nombre= Console.ReadLine();//Guardamos el nombre del nuevo cliente
                    Console.WriteLine("Dame tu dni para seguir con el registro.");
                    dni= Console.ReadLine();//Guardamos el dni del nuevo cliente
                    nombresClientes[i]=nombre;//Asignamos el campo vacio con el nombre del nuevo cliente
                    dniClientes[i]=dni;//Asignamos el campo vacio con el dni del nuevo cliente
                    saldoClientes[i]=0f;//Por defecto tendra 0€
                    registro=true;//Ponemos true el registro para decir que todo fue ok
                }else{
                        break;//Si no cumple con el if es porque no hay hueco disponble y rompemos el for sin completar el registro
                }
            }
            if (registro==true){//Si nos llega registro con valor true entonces le decimos que todo fue ok
                Console.WriteLine("Se ha completado de forma correcta el registro.");
            }else{//En caso contrario le deamos el error.
                Console.WriteLine("Error de resgitro.");
            }
        }
        public static int bajaUsusario(ref float[] saldoClientes, ref string[] nombresClientes, ref string[] dniClientes,int posi){
            //Pasamos arrys con ref para poder midificar esas arrys de forma "global" y posi es la posicion de nuestro cliente en nuestras arrys
            string dni; //Declaramos dni que sera 
            int estadoBorrado;
            //Boolean unBorrado;
            string preguntaDeComprovacion;
            //unBorrado=true;
            estadoBorrado=0;//Con esta varaibale vamos a identificar el estado del borrado
            Console.WriteLine(dniClientes[posi]);
            Console.WriteLine("Dame tu dni para borrarte.");
            dni= Console.ReadLine(); //Le pedimos el dni para poder comprovar que realmente es el.
            Console.WriteLine(@"Estas seguro de que quieres borrar tu ususario.
Responde con Si/s o No/n (por defecto siempre sera no)");
            preguntaDeComprovacion= Console.ReadLine();//Creamos una varaible que usaremos para comprovar que si realment si quiere eleiminar su cuenta
            for (int i =0; i < dniClientes.Length ; i++ ){ 
                    if (preguntaDeComprovacion == "" || preguntaDeComprovacion == "No" || preguntaDeComprovacion =="n"){
                            estadoBorrado=0; //Si es no, pues para paramos la ejecucion de borrado
                            break;
                    }else{
                        if( dni == dniClientes[posi]){ //Si su dni es igual que el dni de la posicion del cliente entnces procedmeos al borrado
                            Console.WriteLine(nombresClientes[posi]);
                            nombresClientes[posi]="";//Asignamos nada al nombre 
                            dniClientes[posi]="";//Asignamos nada el dni
                            saldoClientes[posi]=0f;//Le damos dinero como 0
                            estadoBorrado=1;//Cambio del estado del borrado
                            Console.WriteLine(nombresClientes[posi]);
                            break;
                        }else if (estadoBorrado!=1){
                            estadoBorrado=2;//si no es correcot el dni pones un estado diferente de borrado
                        }
                    }
            }

            if (estadoBorrado==0){//Si cancela pues le damos un mensaje
                Console.WriteLine("Se ha cancelado el proceso de borrado.");
                return posi;//Devuelvo la posicion original de usuario para poder seguir trabanjando en el main
            }else if (estadoBorrado==1){//Si se ha hecho correcto el borrado le mandamos un mensaje
                Console.WriteLine("Se ha borrado de forma correcta el usuario."); 
                return -1;//Duvuelvo un numero negativo para que el bulce del main se reinici y pida indentificacion
            }else if (estadoBorrado==2){//Si se equivoca de dni le damos un mensaje
                Console.WriteLine("Has introducido de froma incorecta el dni, vuelve a intentarlo.");
                return posi;//Si se quivoca devuelvo su posicion para poder seguir trabajando con el cliente
            }
            return posi;//Por si no entra en ninguna lado devuelvo su posicion para poder seguir trabajando con el cliente
        }
        public static int saberQuienEs(ref int quienEs, ref string[] dniClientes){
            //Pasamos arrys con ref para poder midificar esas arrys de forma "global" y posi es la posicion de nuestro cliente en nuestras arrys
            string dni;//Creamos una variable string la cual usaremos para almacenar el dni del cliente
            dni="";
            Boolean autenOk;//Para saber si se ha autenticado de forma correcta
            int indentificador;
            indentificador=-1;//Esta variable se usa para almacernar la posicion del cliente cuando le encontremos
            autenOk=false;
            Console.WriteLine("Bienveido al banco DAW.Escriba su DNI para poder indentificar quien es:");
            dni=Console.ReadLine();
            for (int x =0; x < dniClientes.Length ; x++ ){
                if( dni == dniClientes[x]){//Aqui recomremos la arry de dni y cuando coincida, asignamos la x a indentificador
                    autenOk=true;
                    indentificador=x;
                }
            }
            if (autenOk==true && indentificador>=0){
                return (indentificador);//Si todo fue bien devuelvo la posicion del cliente en mis arrys
            }else{

                return -1;//Si no, un menos -1 el cual provacara que le pregunte otra vez que quien es
            }
        }
            static void Main(string[] args)
            {
                string[] nombresClientes={"Moha","Jordi",""};//Creamos arry para los nombres de los clientes
                string[] dniClientes={"12345678A","12345678B",""};//Creamos arry para los dni's de los clientes
                float[] saldoClientes={68059f,2658f,0};//Creamos arry para el dinero de los clientes
                Boolean salir;//Creamos una varaible para controlar el bucle y con el menu 
                salir= false;//Le damos de valor false
                int quienEs;//declaramos variable de cliente para luegar en ella guardar la posicion del cliente que usaremos para trabajar
                quienEs=-1;//Le damos de valor -1 el cuela nos ayudara a saber que no esta indentificado
                int intentosSession;//Variable de tipos int para poder contar los intentos que hace
                intentosSession=0;//Iniciamos intetnos
                while(quienEs==-1){
                    if (intentosSession>=2){//Si luego de dos intentos no consigue acceder le damos la opcion de que se registre
                        string estaRegistrado;//Variable la cual usaremos para congirmar si se queire registrar o seguir intetnando
                        estaRegistrado="seguir";
                        Console.WriteLine(@"No estas registrado? Escriba seguir o registrarse/r,
por defecto seguira preguntado");
                        if (estaRegistrado=="registrarse" || estaRegistrado=="r"){ //Si se quiere registrar lo mandamos a la funccion de alta con arrys para poder registrarlo
                            altaUsusario(ref saldoClientes,ref nombresClientes, ref dniClientes);
                        }
                    }
                    quienEs=saberQuienEs(ref quienEs, ref dniClientes);//Si no se quiere resgirtar los mandos a la funccion de indentificacion
                    while(salir == false && quienEs>=0){
                        Menu();//Sacamos el menu 
                        Console.WriteLine("Escoja la operacion que desea hacer: ");
                        string num= Console.ReadLine();//Leemos la opcion
                        int opcion = Int32.Parse(num);//La convertimos de int
                            switch(opcion){
                                case 1 :
                                    sacarDinero(ref saldoClientes,quienEs);//Lamamos a la funcion con diferentes para metros, con arrys y la identificacion de quien es para asi poder hacer la operacion de forma correcta
                                    break;
                                ;
                                case 2 :
                                    miCuenta(ref saldoClientes,ref nombresClientes, ref dniClientes, quienEs);//Lamamos a la funcion con diferentes para metros, con arrys y la identificacion de quien es para asi poder hacer la operacion de forma correcta
                                    break;
                                ;
                                case 3 :
                                    ingresar(ref saldoClientes,quienEs);//Lamamos a la funcion con diferentes para metros, con arrys y la identificacion de quien es para asi poder hacer la operacion de forma correcta
                                    break;
                                ;
                                case 4 :
                                    altaUsusario(ref saldoClientes,ref nombresClientes, ref dniClientes);//Lamamos a la funcion con diferentes para metros, con arrys y la identificacion de quien es para asi poder hacer la operacion de forma correcta
                                    break;
                                ;
                                case 5 :
                                    quienEs=bajaUsusario(ref saldoClientes,ref nombresClientes, ref dniClientes, quienEs);//Lamamos a la funcion con diferentes para metros, con arrys y la identificacion de quien es para asi poder hacer la operacion de forma correcta
                                    break;
                                ;
                                case 0 :
                                    salir=true;//Si el cliente quiere salir pues asinamos ture a salir el cual controla al bucle principal
                                    break;
                                ;
                            }
                    }
                    intentosSession++;//Le sumamos 1 cada vez que intenta iniciar session de forma incorrecta y asi llegados a 2 intentos o mas darle la opcion de registrarse
                }                
                
            }

    }
}
