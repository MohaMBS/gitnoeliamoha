﻿using System;

namespace Banco
{
    class Program
    {
        //Creacion del menu que luego se muestra en el MAIN.
        public static void Menu(){
        Console.WriteLine(@"Bienvenido al banco de DAW.
        1.Sacar dinero.
        2.Consultar saldo.
        3.Ingresar dinero.
        4.Alta nuevo usuario.
        5.Baja de usuario.
        0.Salir.");
        }
        public static void sacarDinero(ref float[] saldoClientes, int posi){
            float retira;
            Console.WriteLine("Cuanto desea sacar?");
            retira=Convert.ToSingle(Console.ReadLine());
            saldoClientes[posi]=saldoClientes[posi]-retira;
            Console.WriteLine(saldoClientes[posi]);
            
        }
        public static void ingresar(ref float[] saldoClientes, int posi){
            float retira;
            Console.WriteLine("Cuanto desea ingresar?");
            retira=Convert.ToSingle(Console.ReadLine());
            saldoClientes[posi]=saldoClientes[posi]+retira;
            Console.WriteLine(saldoClientes[posi]);
            
        }
        public static void altaUsusario(ref float[] saldoClientes, ref string[] nombresClientes, ref string[] dniClientes){
            string nombre;
            string dni;
            Boolean registro;
            Boolean sitioLibre;
            sitioLibre=true;
            registro=false;
            for (int i =0; i < dniClientes.Length ; i++ ){
                if (dniClientes[i]==""){
                    if (sitioLibre==true){
                        sitioLibre=false;
                    }else{
                        break;
                    }
                    Console.WriteLine("Dame tu nombre para seguir con el registro.");
                    nombre= Console.ReadLine();
                    Console.WriteLine("Dame tu dni para seguir con el registro.");
                    dni= Console.ReadLine();
                    nombresClientes[i]=nombre;
                    dniClientes[i]=dni;
                    saldoClientes[i]=0f;
                    registro=true;
                }
            }
            if (registro==true){
                Console.WriteLine("Se ha completado de forma correcta el registro.");
            }else{
                Console.WriteLine("Error de resgitro.");
            }
        }
        public static void bajaUsusario(ref float[] saldoClientes, ref string[] nombresClientes, ref string[] dniClientes){
            string dni;
            int estadoBorrado;
            Boolean unBorrado;
            string preguntaDeComprovacion;
            unBorrado=true;
            estadoBorrado=0;
            for (int i =0; i < dniClientes.Length ; i++ ){
                Console.WriteLine(dniClientes[i]);
                if (dniClientes[i]==""){
                    if (unBorrado==true){
                        unBorrado=false;
                    }else{
                        break;
                    }
                    Console.WriteLine("Dame tu dni del ususario que quiere borrar.");
                    dni= Console.ReadLine();
                    Console.WriteLine(@"Estas seguro de que quieres borrar tu ususario.
Responde con Si/s o No/n (por defecto siempre sera no)");
                    preguntaDeComprovacion= Console.ReadLine(); 
                    if (preguntaDeComprovacion == "" || preguntaDeComprovacion == "No" || preguntaDeComprovacion =="n"){
                            estadoBorrado=0;
                    }else{
                        for (int x =0; x < dniClientes.Length ; x++ ){
                            if( dni == dniClientes[x]){
                                Console.WriteLine(nombresClientes[x]);
                                nombresClientes[x]="";
                                dniClientes[i]="";
                                saldoClientes[i]=0f;
                                estadoBorrado=1;
                                Console.WriteLine(nombresClientes[x]);
                            }else if (estadoBorrado!=1){
                                estadoBorrado=2;
                            }
                        }
                    }
                }
            }
            if (estadoBorrado==0){
                Console.WriteLine("Se ha cancelado el proceso de borrado.");
            }else if (estadoBorrado==1){
                Console.WriteLine("Se ha borrado de forma correcta el usuario."); 
            }else if (estadoBorrado==2){
                Console.WriteLine("Has introducido de froma incorecta el dni, vuelve a intentarlo.");
            }
        }
        public static void consulta(ref float[] saldoClientes, int posi){
            Console.WriteLine("Su saldo disponible es de:");
            Console.WriteLine(saldoClientes[posi]);
            
        }
        public static int saberQuienEs(ref int? quienEs, ref string[] dniClientes){
            string dni;
            dni="";
            Boolean autenOk;
            int indentificador;
            indentificador=0;
            autenOk=false;
            Console.WriteLine("Escriba su DNI para poder saber quien es:");
            dni=Console.ReadLine();
            for (int x =0; x < dniClientes.Length ; x++ ){
                if( dni == dniClientes[x]){
                    autenOk=true;
                    indentificador=x;
                }
            }
            if (autenOk==true && indentificador>=0){
                return (indentificador);
            }else{

                return -1;
            }
        }
            static void Main(string[] args)
            {
                string[] nombresClientes={"Moha","Jordi",""};
                string[] dniClientes={"12345678A","12345678B",""};
                float[] saldoClientes={68059f,2658f,0};
                Boolean salir;
                salir= false;
                int? quienEs;
                quienEs=null;
                int intentosSession;
                intentosSession=0;
                while(quienEs==null){
                    if (intentosSession>=2){
                        string estaRegistrado;
                        estaRegistrado="seguir";
                        Console.WriteLine(@"No estas registrado? Escriba seguir o registrarse/r,
                        poer defecto seguira preguntado");
                        if (estaRegistrado=="registrarse" || estaRegistrado=="r"){
                            altaUsusario(ref saldoClientes,ref nombresClientes, ref dniClientes);
                        }
                    }
                    quienEs=saberQuienEs(ref quienEs, ref dniClientes);
                    while(salir == false && quienEs>=0){
                        Menu();
                        Console.WriteLine("Escoja la operacion que desea hacer: ");
                        string num= Console.ReadLine();
                        int opcion = Int32.Parse(num);
                            switch(opcion){
                                case 1 :
                                    sacarDinero(ref saldoClientes,0);
                                    break;
                                ;
                                case 2 :
                                    break;
                                ;
                                case 3 :
                                    ingresar(ref saldoClientes, 0);
                                    break;
                                ;
                                case 4 :
                                    altaUsusario(ref saldoClientes,ref nombresClientes, ref dniClientes);
                                    break;
                                ;
                                case 5 :
                                    bajaUsusario(ref saldoClientes,ref nombresClientes, ref dniClientes);
                                    break;
                                ;
                                case 0 :
                                    salir=true;
                                    break;
                                ;
                            }
                    }
                    intentosSession++;
                }                
                
            }

    }
}
