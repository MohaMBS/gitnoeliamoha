﻿using System;

namespace A02
{
    class Program
    {
        static void menu(){
            Console.Write(@"Aplicacio:
            1. Consulta de caniques
            2. Canvi de nom
            3. Lugar d'on vens.
            4. Canviar d'usuari.
            Què vols fer:");
        }
        public static void canviarNom(ref string[] nomUsuari, int posicionPersona){
            string nombreNuevo;//Aqui lo que hacemos es declara una nueva variable tipo string para poder guradar el nuevo nombre
            Console.WriteLine("Escriba su nuevo nombre completo");
            nombreNuevo= Console.ReadLine();
            nomUsuari[posicionPersona]=nombreNuevo.ToUpper();//Aqui con la posicion que nos pasan al llamar la funcion del client pues asignamos la variable de nombreNuevo y asi lo modificamos
            Console.WriteLine("Su Nombre se cambio a:");
            Console.WriteLine(nomUsuari[posicionPersona]);//Un writeline para que vea como queda el nombre fina.
        }
        public static void canviarCanicas(ref int[] canicasUsuari, int posicionPersona){
            int opcion;
            int cantidad;
            opcion=0;
            cantidad=0;
            Console.WriteLine(@"Menu canicas:
            1.Afegir cacnicas.
            2.Treure canicas.
            3.Sortir.
            Escull el que vos fer:");
            opcion=Int32.Parse(Console.ReadLine());
            switch (opcion){
                case 1:
                    Console.WriteLine("Posa la cantidad que vols introduir:");
                    cantidad=Int32.Parse(Console.ReadLine());
                    if (cantidad > 0){ //Hacemos un control para que solo pueda ingresar mayor a 0
                        canicasUsuari[posicionPersona]=canicasUsuari[posicionPersona]+cantidad;
                        Console.WriteLine("Ara tens un total de "+canicasUsuari[posicionPersona]);
                    }else{
                        Console.WriteLine("No es pot afegir 0");
                    }
                    break;
                case 2:
                    Console.WriteLine("Posa la cantidad que vols introduir:");
                    cantidad=Int32.Parse(Console.ReadLine());
                    if (cantidad > 0){ //Hacemos un control para que solo pueda ingresar mayo 0
                        canicasUsuari[posicionPersona]=canicasUsuari[posicionPersona]-cantidad;
                        Console.WriteLine("Ara tens un total de "+canicasUsuari[posicionPersona]);
                    }else{
                        Console.WriteLine("No es pot treure 0");
                    }
                    break;
                case 3:
                    break;
                default:
                        Console.WriteLine("Aquesta opcio no existeix!\nSortint del menu de canicas...");
                    break;
            }
        }
        
        public static int quineEres(ref string[] nomUsuari, int posicionPersona){
            string nom;
            nom="";
            Boolean autenOk;//Para saber si se ha autenticado de forma correcta
            int indentificador;
            indentificador=-1;//Esta variable se usa para almacernar la posicion del cliente cuando le encontremos
            autenOk=false;
            Console.WriteLine("Dame tu nombre para indentificar quien es:");
            nom=Console.ReadLine();
            for (int x =0; x < nomUsuari.Length ; x++ ){
                if( nom.ToUpper() == nomUsuari[x]){//Aqui recomremos la arry de dni y cuando coincida, asignamos la x a indentificador
                    autenOk=true;
                    indentificador=x;
                }
            }
            if (autenOk==true && indentificador>=0){
                return (indentificador);//Si todo fue bien devuelvo la posicion del cliente en mis arrys
            }else{

                return -1;//Si no, un menos -1 el cual provacara que le pregunte otra vez que quien es
            }
        }
        static void consultacanicas(ref int[] canicasUsuari, int posicionPersona){
            Console.WriteLine("El teu numero de caniques és: " +canicasUsuari[posicionPersona]);
            canviarCanicas(ref canicasUsuari, posicionPersona);
        }
        static void donVens(ref string[] lugarUsuari, int posicionPersona){
            Console.WriteLine("Vens de: " +lugarUsuari[posicionPersona]);
        }
        static void Main(string[] args)
        {
            string[] nomUsuari={"JOAN","NIL","ROC", "THIAGO", "NICO", "NOA", "GERARD ", "NOR "};
            int[] canicasUsuari={12, 3, 25, 31, 6, 11, 21, 9 };
            string[] lugarUsuari={"VILANOVA", "CANYELLES", "VILANOVA", "SITGES", "CUNIT", "VILANOVA", "KAZAHKSTAN", "CUBELLES" };
            int posicionPersona;
            posicionPersona=-1;
            while (posicionPersona == -1)
            {
                posicionPersona=quineEres(ref nomUsuari, posicionPersona);
                while(posicionPersona >= 0){
                    menu();
                    string numero= Console.ReadLine();
                    int opcio = Int32.Parse(numero);
                    switch(opcio){
                        case 1:
                            consultacanicas(ref canicasUsuari, posicionPersona);
                            break;
                        case 2:
                            canviarNom(ref nomUsuari, posicionPersona);
                            break;
                        case 3:
                            donVens(ref lugarUsuari,  posicionPersona);
                            break;
                        case 4:
                            posicionPersona=quineEres(ref nomUsuari, posicionPersona);
                            break;
                        default:
                            Console.WriteLine("Aquesta opcio no existeix!\nIntrodueix un altre.");
				        break; }
                }
            }
        }
    }
}
